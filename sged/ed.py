import networkx as nx
import sys

from sged.exploration_tree import ExplorationTree
from sged.cost import Costs

from sortedcontainers import SortedListWithKey

settings = {
    "max_open_set_size": -1
    , "log_to_console": False
    , "max_children_in_er": -1
    , "penalize_superfluous_edges": True    # penalty for edges (v^2_1, v^2_2) \in E(G2) such that
                                            # there exists v^1_1, v^1_2: f(v^1_1)=v^2_1 && f(v^1_2)=v^2_2 && (v^1_1, v^1_2) not \in E(G1)
    , "max_consecutive_inserts": 1
    # , "max_consecutive_deletes": -1
}

graph_cache = []


def sged(g1, g2, node_costs=Costs(), edge_costs=Costs(), user_settings={}):

    settings.update(user_settings)

    # graphs = first_smaller(g1, g2)
    graphs = [g1, g2]
    # graph_cache = [GraphCache(g1), GraphCache(g2)]
    open_set = SortedListWithKey(key=open_set_sort_key)
    touched_paths = set()

    nodes = [sorted(list(graphs[0].nodes)), sorted(list(graphs[1].nodes))]

    root = ExplorationTree(graphs, node_costs=node_costs, edge_costs=edge_costs, settings=settings)

    # for node1 in nodes[0] + [None]:
    #     for node2 in nodes[1] + [None]:
    #         if node1 or node2:
    #             open_set.append(root.add_operation_child([node1, node2]))

    # for node1 in [nodes[0][0]]:
    node1 = sorted(pick_seed_nodes(nodes, cnt=1, node_costs=node_costs))[0]
    if settings["log_to_console"]:
        print("Starting with node: {}".format(node1))

    # for node1 in sorted(pick_seed_nodes(graphs, node_costs=node_costs)):
    for node2 in nodes[1] + [None]:
        if node1 or node2:
            add_to_open_set(open_set, root.add_operation_child([node1, node2]))
            # open_set.append(root.add_operation_child([node1, node2]))

    min_mappings = []
    cnt_tested = 0
    while len(open_set) > 0:
        cnt_tested +=1

        ix = 0
        # ix = pick_min_scoring_path(open_set)
        et = open_set[ix]

        # if cnt_tested == 20000:
        #     return min_mappings
        # if len(min_mappings) > 100:
        #     return min_mappings
        # if et.get_depth() >60:
        #     return min_mappings

        if cnt_tested % 100 == 0:
            print(cnt_tested)
        if settings["log_to_console"]:
            print("Tested solutions: {} (et:{}, path:{}, min dist: {}, et size: {})".format(
                cnt_tested,
                et,
                et.get_path(),
                min_mappings[0]["distance"] if len(min_mappings) > 0 else "",
                len(root)))

        if et.get_depth() > 0 and 0 <= settings["max_children_in_er"] <= len(et.get_children()):
            remove_current_from_os(open_set)
            continue


        if len(min_mappings) > 0 and et.get_path_score_optimistic() > min_mappings[0]["distance"]:
                print("Tested solutions: {}".format(cnt_tested))
                return min_mappings

        # TODO - define possibility to specify which or whether this should be semi-global alignment
        processed = et.get_processed()

        if len(processed[0]) == len(graphs[0].nodes()):
            new_score = et.get_path_score()

            if len(min_mappings) == 0 or new_score <= min_mappings[0]["distance"]:
                if len(min_mappings) > 0 and new_score < min_mappings[0]["distance"]:
                    min_mappings.clear()
                min_mappings.append({
                    "mapping": et.get_path(),
                    "distance": new_score
                })
                if settings["log_to_console"]:
                    print("New mapping:{} ({})".format(et, new_score))

            # open_set.remove(et)
            remove_current_from_os(open_set)
            continue

        #extend path
        ns = []

        path = et.get_path()
        path_dicts = get_dicts_from_path(path)
        processed = [set(), set()]

        for p in path:
            if p[0] is not None:
                processed[0].add(p[0])
            if p[1] is not None:
                processed[1].add(p[1])

        # print('processed1 {} processed2 {}'.format(len(processed[0]), len(processed[1])))

        nodes_to_add = []
        for i in range(2):
            v1_candidates = {}
            for (v1, v2) in nx.edge_boundary(graphs[i], processed[i]):

                if v1 not in v1_candidates:
                    # v1_candidates[v1] = get_candidate_nodes((v1, v2), graphs[i], graphs[1-i], graph_cache[i], graph_cache[i-1], processed[i], processed[1-i], path_dicts[i])
                    v1_candidates[v1] = get_candidate_nodes((v1, v2), graphs[i], graphs[1 - i], processed[i], processed[1 - i], path_dicts[i])
                    if len(v1_candidates[v1]) == 0:
                        v1_candidates[v1] = graphs[1 - i].nodes()

                nodes2 = v1_candidates[v1]



                # last_mapped_in_second = None
                # found = False
                # for j in range(len(path)):
                #     if path[j][0] and path[j][1]:
                #         last_mapped_in_second = path[j][1-i]
                #     if path[j][i] == v1:
                #         found = True
                #         break
                # # TODO when the following raise exception is present, the code fails in "open_set.append(et.add_operation_child([v2, node2]))" on debugging
                # # if not found:
                # #     raise Exception('Node not found in path')
                #
                # nodes2 = []
                # if last_mapped_in_second:
                #     # find neighbors of the last node which was mapped in the second graph (i.e. not inserted or deleted)
                #     nodes2 = sorted(list(nx.all_neighbors(graphs[1 - i], last_mapped_in_second)))
                # else:
                #     # there might not be a mapped node when only deletions from G2 occurred up to now
                #     nodes2 = sorted(graphs[1 - i].nodes())

                for node2 in list(nodes2) + [None]:

                    if i == 1 and node2 is None and len(path) > settings["max_consecutive_inserts"]:
                        #test if there are too many consecutive inserts and if yes, do not add another one
                        all_none = True
                        for ix_ci in range(settings["max_consecutive_inserts"]):
                            if path[-(ix_ci+1)][0] is not None:
                                all_none = False
                                break
                        if all_none:
                            continue

                    if node2 not in processed[1-i]:
                        if i == 0:
                            op = [v2, node2]
                        else:
                            op = [node2, v2]

                        # new_node = et.add_operation_child(op)
                        # open_set.append(new_node)
                        if not path_and_op_touched(touched_paths, path, op):
                            new_node = et.add_operation_child(op)
                            nodes_to_add.append(new_node)
                            add_touched_paths(touched_paths, new_node.get_path())


                            # open_set.append(new_node)


        open_set.remove(et)
        for node in nodes_to_add:
            add_to_open_set(open_set, node)

        prune_os(open_set, settings["max_open_set_size"])

    return min_mappings

def get_dicts_from_path(path):
    dicts = [{},{}]
    for op in path:
        if op[0] is not None:
            dicts[0][op[0]] = op[1]
        if op[1] is not None:
            dicts[1][op[1]] = op[0]

    return dicts


def get_candidate_nodes(e, g1, g2, mapped1, mapped2, map1):
# def get_candidate_nodes(e, g1, g2, gc1, gc2, mapped1, mapped2, map1):
    candidates2 = set()
    q = [e[0]]
    processed = set()
    cnt  = 0
    while len(q) > 0:
        cnt+=1
        n1 = q[0]
        q.remove(n1)
        processed.add(n1)
        if map1[n1] is not None:
            for nb2 in nx.all_neighbors(g2, map1[n1]):
            # for nb2 in gc2.get_neighbors(map1[n1]):
            #     if nb2 not in mapped2:
                    candidates2.add(nb2)
        else:
            for nb in nx.all_neighbors(g1, n1):
            # for nb in gc1.get_neighbors(n1):
                if nb not in processed and nb in mapped1:
                    q.append(nb)

    # if (len(candidates2)>10):
    #     print("processed when looking for candidtes2: {}".format(cnt))
    #     print("candidtes2: {}".format(len(candidates2)))
    return candidates2


def remove_current_from_os(open_set):
    del open_set[0]

def prune_os(open_set, target_size):
    if len(open_set) > settings["max_open_set_size"]:
        threshold = settings["max_open_set_size"]
        # min_score = open_set_sort_key(open_set[0])
        # while open_set_sort_key(open_set[threshold]) <= min_score:
        #     threshold += 1
        del open_set[threshold:]
        # print("threshold: {}".format(threshold))

def open_set_sort_key(x):
    return x.get_path_score_optimistic() + x.get_expected_path_score()


def pick_seed_nodes(nodes, cnt = 1, node_costs=Costs()):
    costs = []
    for n1 in nodes[0]:
        for n2 in nodes[1]:
            costs.append([n1, n2, node_costs.update([n1, n2])])
    costs.sort(key=lambda x: x[2])

    seeds = []
    for i in range(len(costs)):
        if costs[i][0] not in seeds:
            seeds.append(costs[i][0])
        if len(seeds) == cnt:
            break

    return seeds


def first_smaller(g1, g2):
    return [g2, g1] if len(g1.nodes()) > len(g2.nodes()) else [g1,g2]


def pick_min_scoring_path(open_set):
    return 0
    min_score = sys.float_info.max
    min_ix = -1
    min_depth = -1
    for ix in range(len(open_set)):
        # score = open_set[ix].get_path_score() + open_set[ix].get_expected_path_score()
        score = open_set[ix]["expected score"]
        depth = open_set[ix]["node"].get_depth()
        if score < min_score or (score == min_score and depth > min_depth):
            min_score=score
            min_ix = ix
            min_depth = depth

    return min_ix


def add_to_open_set(os, n):
    os.add(n)
    return
    os.append({
        "node": n,
        "expected score": n.get_path_score() + n.get_expected_path_score()
    })
    return
    ix = 0
    n_score = n.get_path_score() + n.get_expected_path_score()
    n_depth = n.get_depth()
    while ix < len(os):
        score = os[ix].get_path_score() + os[ix].get_expected_path_score()
        depth = os[ix].get_depth()
        if n_score < score or (n_score == score and n_depth > depth):
            break
        ix += 1
    os.insert(ix, n)


def serialize_operation(op):
    # TODO make sure that two operations can't have the same code (eg. a-|ab vs a|-ab)
    return (op[0] if op[0] else 'NONE') + "-" + (op[1] if op[1] else 'NONE')


def serialize_path(path):
    s_path = [serialize_operation(op) for op in path]
    s_path.sort()
    return "_".join(s_path)


def path_and_op_touched(paths, path, op):
    new_s_path = serialize_path(path + [op])
    # if new_s_path in paths:
    #     print("new:{}".format(new_s_path, paths))
    return new_s_path in paths


def add_touched_paths(paths, path):
    s_path = serialize_path(path)
    # ixs_to_remove = []
    # for ix in range(len(paths)):
    #     if s_path.find(paths[ix]) >=0:
    #         ixs_to_remove.append(ix)
    # for ix in ixs_to_remove[::-1]:
    #     del paths[ix]
    paths.add(s_path)
