import networkx as nx
import numpy as np
import sys

from .cost import Costs
from .operation import Operation
from .mapping import Mapping
from .graph import Graph



def merge_two_dicts(x, y):
    z = x.copy()   # start with x's keys and values
    z.update(y)    # modifies z with y's keys and values & returns None
    return z


def initExplorationGraph():

    sys.setrecursionlimit(20000)

    ExplorationGraph.graphs = []  # static shared by
    ExplorationGraph.use_cache = True
    ExplorationGraph.settings = {}
    ExplorationGraph.op_track = {}  # keeps track of operations across the exploration tree
    ExplorationGraph.node_costs = Costs()
    ExplorationGraph.edge_costs = Costs()

    ExplorationGraph.mappings_track = {}
    ExplorationGraph.mappings = set()


class ExplorationGraph(object):

    def __init__(self, graphs=None, operation=[None,None], node_costs=None, edge_costs=None, settings={}):

        if len(settings):
            ExplorationGraph.settings = settings

        if operation[0] is None and operation[1] is None:
            self.is_root = True
        else:
            self.is_root = False
        self.set_operation(operation)

        if graphs is None:
            graphs = ExplorationGraph.graphs
        else:
            if len(graphs) == 2:
                self.set_graphs(graphs)
            else:
                raise Exception('Wrong number of graphs passed to the ExplorationTree object')

        if node_costs:
            ExplorationGraph.node_costs = node_costs
            
        if edge_costs:
            ExplorationGraph.edge_costs = edge_costs

        self.pred = []
        self.succ = []
        
        # self.parent = None
        # node_score gets the score of a node considering the worst possible scenario,
        # i.e. not yet mapped neibhors of a node are treated as nodes which will be mapped to a non-neighbor
        # and only when it turns out that two neighboring nodes stay neighbors in the target graph the
        # score will be corrected
        self.node_score = 0
        # node_score_optimistic considers the best possible case, i.e. ale neighbors of a mapped node are
        # considered to be neighbors of the same node even after mapping and only when all neibhors of a node
        # are mapped, this will be corrected
        self.node_score_optimistic = 0
        self.path_score = 0
        self.path_score_optimistic = 0
        # self.set_depth(0)
        self.cache = {} # Storing once computed stuff such as path


    def copy(self):
        # node = copy.deepcopy(self) # this copies over all predecessors and successort
        node = ExplorationGraph(operation=self.get_operation())

        node.node_score = self.node_score
        node.node_score_optimistic = self.node_score_optimistic
        node.path_score = self.path_score
        node.path_score_optimistic = self.path_score_optimistic

        node.pred = [p for p in self.pred]

        # node.clear_cache()
        return node

    def __repr__(self):
        return get_node_repr(self)

    def __str__(self):
        return get_node_repr(self)

    def __len__(self):
        return sum([len(ch) for ch in self.succ]) if len(self.succ) > 0 else 1

    def set_graphs(self, graphs):
        ExplorationGraph.graphs = [Graph(graphs[0]), Graph(graphs[1])]

    def set_operation(self, op):
        self.operation = op if isinstance(op, Operation) else Operation(op)

    def untrack_node_op(self):
        ExplorationGraph.op_track[self.operation].remove(self)

    def track(self):
        self.track_node_op()
        self.track_node_mapping()

    def track_node_op(self):

        if self.operation is None:
            return

        if self.operation not in ExplorationGraph.op_track:
            ExplorationGraph.op_track[self.operation] = set()
        ExplorationGraph.op_track[self.operation].add(self)

    def track_node_mapping(self):
        ExplorationGraph.mappings.add(self.get_mapping().serialize())

    def add_succ(self, node):
        assert isinstance(node, ExplorationGraph)
        # node.add_pred(self)
        self.succ.append(node)
        node.track()

    def get_succs(self):
        return self.succ

    def get_last_processed(self):
        last = [None,None]
        node = self
        while not last[0] and not last[1] and node:
            for i in range(2):
                if not last[i] and node.operation[i]:
                    last[i] = node.operation[i]
            node = node.get_parent()
        return last

    def get_processed(self):
        """
        Get list of already mapped, inserted or deleted nodes
        :return:
        """
        node = self
        processed = [[], []]
        while node:
            if node.operation[0]:
                processed[0].append(node.operation[0])
            if node.operation[1]:
                processed[1].append(node.operation[1])
            node = node.get_parent()
        return processed

    def add_operation(self, op):

        if not isinstance(op, Operation):
            op = Operation(op)

        new_nodes = []
        new_serialized_mps = []

        # pred_candidates = ExplorationGraph.op_track[op] if op in ExplorationGraph.op_track else []
        # cmptbl_pred_candidates = [c for c in pred_candidates if node_compatibility(self, c)]
        # cmptbl_pred_candidates_ext = []
        # for cpc in cmptbl_pred_candidates:
        #     cmptbl_pred_candidates_ext += get_extended_compatible_mappings(self, cpc)
        #
        # for cpce in cmptbl_pred_candidates_ext:
        #
        #     if len(cpce.get_mapping().get_set(0).symmetric_difference(self.get_mapping().get_set(0))) <= 4:
        #         continue
        #
        #     # copy the current node (including the list of its predecessors)
        #     node = self.copy()
        #
        #     if node.get_mapping().exists_in_mapping(cpce.get_mapping()):
        #         continue
        #
        #     # merge the new node (i.e. add new predecessor and compute the cost for that node based on the merged)
        #     node.add_pred(cpce, True)
        #
        #     if node.mapping_in_eg():
        #         continue
        #
        #     for p in self.get_preds():
        #         p.succ.append(node)
        #     node.track()
        #     new_nodes.append(node)

        # Add operation to the mapping represented by the current node (i.e. no merging)
        # if it was not already added during the merging
        node = ExplorationGraph(operation=op)
        node.add_pred(self, False)
        # in the following line the second part of the and operation prevents finding mapping of A-B-C-F and A-D-E-F
        # when penalties set in such a way that A-A, B:, C:, :D, :E, F:F is optimal. In such case when having the partial
        # mapping "A:A, B:" it does not allow C: because that is already a part of another mapping with F:None (going from
        # the other side)
        # if not node.mapping_in_eg() and not node.get_mapping().exists_in_mappings([n.get_mapping() for n in cmptbl_pred_candidates_ext]):
        if not node.mapping_in_eg():
            self.add_succ(node)
            node.set_scores()
            new_nodes.append(node)

        for n in new_nodes:
            s = n.get_mapping().serialize()
            if s not in ExplorationGraph.mappings_track:
                ExplorationGraph.mappings_track[s] = []
            ExplorationGraph.mappings_track[s].append(n)


        return new_nodes

    def clear_cache(self):
        self.cache.clear()

    def mapping_in_eg(self):
        return self.get_mapping().serialize() in ExplorationGraph.mappings


    def add_pred(self, pred, merging):
        if merging and not ExplorationGraph.settings['use_adjecency_matrix_scoring']:
            self.merge_score(pred)

        self.pred.append(pred)
        self.clear_cache()

        if merging and ExplorationGraph.settings['use_adjecency_matrix_scoring']:
            self.set_scores()

    def merge_score(self, pred): #TODO update add_pred_score

        score_merge = get_merge_cost(self, pred)

        self.path_score = score_merge['pessimistic']
        self.path_score_optimistic = score_merge['optimistic']

        # assert (self.path_score_optimistic <= self.path_score)


    # def get_parent(self):
    #     return self.parent

    def get_preds(self):
        return self.pred

    # def set_depth(self, depth):
    #     self.depth = depth

    # def get_depth(self):
    #     return self.depth    

    def get_node_score(self):
        return self.node_score

    def get_node_score_optimistic(self):
        return self.node_score_optimistic

    def get_mapping_score(self):
        return self.path_score

    def get_mapping_score_optimistic(self):
        return self.path_score_optimistic

    def get_expected_mapping_score(self):
        return 0

    def get_expected_optimistic_score(self):
        return self.get_mapping_score_optimistic() + 0

    def get_scores_insert(self):
        """
        Operation which inserts a node N2 from G2 into G1.
        
        Cost includes insertion of the node itself and insertion of each edge connecting N2 to its 
        neighbors NN2 onto which a node from G1 is mapped or which was inserted. I.e. if there is an edge from N2 to a 
        neighbor onto which nothing from G1 is mapped, this edge is not penalized because it will simply
        not be in the resulting graph and will thus not be inserted.

        One more cost to be added is for possible deletion of superfluous edges (if it is turned on in the settings) in case when N2
        connects to NN2 where f^-1(NN2) \in G1.
        :return: 
        """

        # TODO !!!!!! consider superfluous edges in the pessimistic score and possibly fix them when all
        # TODO !!!!!! neighbors of given node are mapped or when everything is mapped

        optimistic = 0 # optimistic score
        pessimistic = 0 #pessimistic score
        
        op = self.operation

        pessimistic += ExplorationGraph.node_costs.insert(op)
        optimistic += ExplorationGraph.node_costs.insert(op)

        # cost for adding edge between the node and all already mapped nodes
        mapped1 = self.get_mapping().get_list(1)
        # mapped_dict1 = self.get_mapping().get_dict(1)
        for nb1 in nx.all_neighbors(ExplorationGraph.graphs[1].get_graph(), op[1]):
            if nb1 in mapped1:
                pessimistic += ExplorationGraph.edge_costs.insert([op[1], nb1])
                optimistic += ExplorationGraph.edge_costs.insert([op[1], nb1])

                # if mapped_dict1[nb1] is not None and ExplorationGraph.settings["penalize_superfluous_edges"]:
                #     pessimistic += ExplorationGraph.edge_costs.delete([op[1], nb1])
                #     optimistic += ExplorationGraph.edge_costs.insert([op[1], nb1])
            
        return {"optimistic": optimistic, "pessimistic": pessimistic}

    def get_scores_delete(self):
        """
        Operation which deletes a node N1 from G1.

        Cost includes cost for deleting N1 and its edges in G1. The costs depend on the mapping of neighbors
         of NN1:

         - NN1 is mapped to a node in G2 -> the cost for removal of the edge (N1. NN1) was already included
         in the pessimistic score when mapping NN1. However we must add cost to the optimistic score, because
         when mapping NN1, optimistic score assumes that all its neighbors in G1 will be mapped to neighbors in G2.
         This turns out not to be true since N1 is just being deleted.

         - NN1 was not yet mapped -> add cost to both optimistic and pessimistic scores for deletion of (N1, NN1)
         since this edge will definitely not be present in the resulting graph

         - NN1 was deleted -> no cost needs to be added since this cost was already added when removing NN1 (see
         the previous point)

        :return:
        """

        optimistic = 0  # optimistic score
        pessimistic = 0  # pessimistic score

        op = self.operation

        # cost for removing a node
        pessimistic += ExplorationGraph.node_costs.delete(op)
        optimistic += ExplorationGraph.node_costs.delete(op)

        # cost for removing its connecting edges
        mapped = self.get_mapping().get_list(0)
        mapped_dict = self.get_mapping().get_dict(0)
        for n in nx.all_neighbors(ExplorationGraph.graphs[0].get_graph(), op[0]):

            if n not in mapped:
                pessimistic += ExplorationGraph.edge_costs.delete((op[0], n))
                optimistic += ExplorationGraph.edge_costs.delete((op[0], n))

            else:
                if mapped_dict[n] is not None:
                    optimistic += ExplorationGraph.edge_costs.delete((op[0], n))

        return {"optimistic": optimistic, "pessimistic": pessimistic}

    def get_scores_update(self):
        """
        Operation which mapps a node N1 from G1 to f(N1)=N2 in G2.

        Cost includes the node update cost plus. Edge costs depend on the mapping of neighbors NN1 of N1 in G1
        and NN2 of N2 in G2:

        - CASE 1: f(NN1) is not known, i.e. NN1 was not mapped yet. We add cost to the pessimistic score since the pessimistic
        variant is that (f(N1),f(NN1) not \in G2

        - CASE 2: f(NN1) is known AND not null AND (f(N1),f(NN1)) \in G2 then we decrease pessimistic score because this edge was presumed
        to be deleted when mapping NN1, but now we see it was not correct. No need to change the optimistic score, because
        that counted with this situation.

        - CASE 3: f(NN1) is known AND not null AND (f(N1),f(NN1)) not \in G2 then we increase optimistic score because this edge was presumed
        not to be deleted when mapping NN1, but now we see it is deleted. No need to change the pessimistic score, because
        that counted with this situation.

        - CASE 4: f(NN1) is known AND null, i.e. f(NN1) was deleted then we don't need to add any cost since it has already
        been added to both optimistic and pessimistic score when deleting NN1

        - CASE 5: SUPERFLUOUS EDGES - if set, we need also to consider neighbors f(N1). It can happen that it has a neighbor
        NN2 where (N1, f^-1(NN2)) not in G1 (but (f(N1),NN2) \in G2). Then we need to add cost for edge insertion
        into both pessimistic and optimistic score (since the insertion "already happened" because both nodes are
        already mapped)

        :return:
        """

        # TODO !!!!!! consider superfluous edges in the pessimistic score and possibly fix them when all
        # TODO !!!!!! neighbors of given node are mapped or when everything is mapped

        optimistic = 0  # optimistic score
        pessimistic = 0  # pessimistic score

        op = self.operation

        # cost for mapping the nodes themselves (not including edges)
        pessimistic += ExplorationGraph.node_costs.update(op)
        optimistic += ExplorationGraph.node_costs.update(op)

        for nb0 in nx.all_neighbors(ExplorationGraph.graphs[0].get_graph(), op[0]):

            nb0_mapped = nb0 in self.get_mapping().get_dict(0)
            nb1 = self.get_mapping().get_dict(0)[nb0] if nb0_mapped else None

            if not nb0_mapped:
                # CASE 1
                pessimistic += ExplorationGraph.edge_costs.delete((op[0], nb0))
            else:
                # if N is mapped and f(N) and f(X) are neighbors in G2 remove cost for (N,X)
                if nb1 is not None:
                    if nb1 in nx.all_neighbors(ExplorationGraph.graphs[1].get_graph(), op[1]):
                        # CASE 2
                        # decrease the score by the cost of edge removal since this was "incorrectly" added when introducing mapping of N
                        pessimistic -= ExplorationGraph.edge_costs.delete((op[0], nb0))
                    else:
                        # CASE 3
                        optimistic += ExplorationGraph.edge_costs.delete((op[0], nb0))

                # else:
                    #CASE 4
                    # pessimistic += ExplorationGraph.edge_costs.insert((op[0], nb0))

                # update the optimistic score by finding out whether any already mapped neighbor N of the existing node X
                # already has all of its neighbors mapped and if yes increase cost by adding penalties for neighbors NN of N
                # which are not neighbors in G2, ie. (f(N),f(NN)) not in E(G2)
                if nb1:
                    # if the neighbor nb0 is mapped to a node nb1 (and therefore not deleted)

                    neighbor_score_update = 0

                    all_mapped = True
                    neighbors_mappings = []
                    for nnb0 in nx.all_neighbors(ExplorationGraph.graphs[0].get_graph(), nb0):

                        # [mapped, nnb1] = node_mapping(nnb0, path, 0)
                        mapped = nnb0 in self.get_mapping().get_dict(0)
                        nnb1 = self.get_mapping().get_dict(0)[nnb0] if mapped else None

                        neighbors_mappings.append([nnb0, nnb1])
                        if not mapped:
                            all_mapped = False
                            break
                    if all_mapped:
                        # add cost for deleting edges, i.e. cost for mapping neighbors of nb0 to non-neighbors of nb1
                        for [nnb0, nnb1] in neighbors_mappings:
                            if not nnb1 or (nnb1 not in nx.all_neighbors(ExplorationGraph.graphs[1].get_graph(), nb1)):
                                # the neighbor nnb0 of nb0 was deleted
                                # or the neighbor nnb0 was mapped to the node nnb1 which is not neighbor of nb1 in G2
                                neighbor_score_update += ExplorationGraph.edge_costs.delete([nb0, nnb0])

                    optimistic += neighbor_score_update
                    # if neighbor_score_update > 0:
                    #     update_optimistic_score(self, nb0, neighbor_score_update)

        if ExplorationGraph.settings["penalize_superfluous_edges"]:
            neighbor_score_update = 0

            md1 = self.get_mapping().get_dict(1)
            for nb1 in nx.all_neighbors(ExplorationGraph.graphs[1].get_graph(), op[1]):

                if not nb1 in md1:
                    continue

                nb0 = md1[nb1]
                if nb0 is not None and nb0 not in nx.all_neighbors(ExplorationGraph.graphs[0].get_graph(), op[0]):
                    # the penalties in situation when the neighbor was inserted is already accounted for when treating node insertion
                    neighbor_score_update += ExplorationGraph.edge_costs.insert([op[1], nb1])

            optimistic += neighbor_score_update
            pessimistic += neighbor_score_update

       
        return {"optimistic": optimistic, "pessimistic": pessimistic}

    def set_mapping_scores(self):

        mapping = self.get_mapping()

        deleted = sorted([i for i in mapping.get_inserted(0)])
        inserted = sorted([i for i in mapping.get_inserted(1)])
        updated = self.get_mapping().get_mapping_upd()

        g = [ExplorationGraph.graphs[0], ExplorationGraph.graphs[1]]

        mlu = [mapping.get_list_upd(0), mapping.get_list_upd(1)]
        ml = [mapping.get_list(0), mapping.get_list(1)]

        ix_updated = []
        am = []
        am_upd = []
        for i in range(2):

            ix_updated.append([g[i].get_node_ix(op[i]) for op in updated])
            # ix_deleted = [[g[i].get_node_ix(x)] for x in deleted]
            # ix_inserted = [[g[i].get_node_ix(x)] for x in inserted]

            am.append(g[i].get_adj_matrix())
            am_upd.append(am[i][np.ix_(ix_updated[i], ix_updated[i])])

        # perm = [0] * len(ix_updated[0])
        # for ix in range(len(updated)):
        #     n1 = updated[ix][1]
        #     perm[mlu[1].index(n1)] = ix

        # https://stackoverflow.com/questions/10936767/rearranging-matrix-elements-with-numpy
        # a = np.arange(100)a.shape = (10,10)
        # a = np.arange(100)a.shape = (10,10)
        # a[np.ix_([3,6], [1,9])]

        # am1_rearranged = am[1][:, perm][perm]

        # diff = am_upd[0] - am1_rearranged
        diff = am_upd[0] - am_upd[1]

        cnt_edges_del = np.count_nonzero(diff == 1)
        cnt_edges_ins = np.count_nonzero(diff == -1) if ExplorationGraph.settings["penalize_superfluous_edges"] else 0

        nodes_score = sum([ExplorationGraph.node_costs.update(op) for op in updated])
        nodes_score += sum([ExplorationGraph.node_costs.delete(n) for n in deleted])
        nodes_score += sum([ExplorationGraph.node_costs.insert(n) for n in inserted])

        edges_score = ExplorationGraph.edge_costs.delete(None) * cnt_edges_del / 2 # divided by 2 since the matrix is symmetric
        edges_score += ExplorationGraph.edge_costs.insert(None) * cnt_edges_ins / 2

        #number of deleted edges for every deleted node
        gg = [g[0].get_graph(), g[1].get_graph()]
        cnt_edges = 0
        inspected = set()
        for n in deleted:
            # cnt_edges += len(list(nx.all_neighbors(gg[0], n)))
            inspected.add(n)
            for nn in nx.all_neighbors(gg[0], n):
                if nn not in inspected:
                    cnt_edges += 1
        edges_score += ExplorationGraph.edge_costs.delete(None) * cnt_edges

        cnt_edges = 0
        inspected = set()
        for n in inserted:
            # cnt += sum([ 1 for nn in nx.all_neighbors(gg[1], n) if nn in ml[1] ])
            inspected.add(n)
            for nn in nx.all_neighbors(gg[1], n):
                if nn in ml[1] and nn not in inspected:
                    cnt_edges += 1

        edges_score += ExplorationGraph.edge_costs.insert(None) * cnt_edges

        self.path_score_optimistic = nodes_score + edges_score
        self.path_score = self.path_score_optimistic

        # cnt_possibly_del_edges = np.count_nonzero(am[0][ix_updated[0]] == 1) - np.count_nonzero(am_upd[0] == 1)

        # self.path_score = self.path_score_optimistic + cnt_possibly_del_edges * ExplorationGraph.edge_costs.delete(None)
        

        # assert (self.path_score_optimistic <= self.path_score)

    def set_scores(self):

        if ExplorationGraph.settings['use_adjecency_matrix_scoring']:
            self.set_mapping_scores()
        else:

            if self.operation[0] or self.operation[1]:

                if not self.operation[0]:
                    scores = self.get_scores_insert()

                elif not self.operation[1]:
                    scores = self.get_scores_delete()

                else:
                    scores = self.get_scores_update()

                self.node_score += scores["pessimistic"]
                self.node_score_optimistic += scores["optimistic"]

            self.path_score = self.node_score + sum([p.get_mapping_score() for p in self.get_preds()])
            self.path_score_optimistic = self.node_score_optimistic + sum([p.get_mapping_score_optimistic() for p in self.get_preds()])

            # assert(self.path_score_optimistic <= self.path_score)

    def get_operation(self):
        return self.operation

    def get_new_mapping(self):

        # if self.is_root:
        #     return Mapping([])

        mapping = set()
        for pred in self.get_preds():
            mapping = mapping.union(set(pred.get_mapping().get_mapping()))
        mapping.add(self.get_operation())
        return Mapping(list(mapping))

        # node = self
        # mapping = set()
        # q = [self]
        # encountered = []
        # while len(q) > 0:
        #     n = q.pop()
        #     if id(n) not in encountered:
        #         encountered.append(id(n))
        #     else:
        #         continue
        #     op = n.get_operation()
        #     if op.is_valid():
        #         mapping.add(op)
        #     q += n.get_preds()
        # return  Mapping(list(mapping))

    def get_mapping(self):


        if ExplorationGraph.use_cache:
            cache_key = "mapping_"# + "_".join(list(locals().values())[1:])
            if cache_key in self.cache:
                return self.cache[cache_key]


        rv = self.get_new_mapping()


        if ExplorationGraph.use_cache:
            self.cache[cache_key] = rv

        return rv

    def get_consecutive_indels(self, op_ix):

        def aux_consecutive(n):
            """
                :return: Pair consisting of longest sequence of insertions ending in this node and longest sequence of
                insertions encountered ever. It could happen that there are multiple sequences of insertions in
                one path.
            """
            op = n.get_operation()
            if op[0] is None and op[1] is None:
                return (0,0)
            else:
                (current_score, max_score) = max([aux_consecutive(p) for p in n.get_preds()], key=lambda p:p[1])
                if op[op_ix] is None:
                    current_score += 1
                    if current_score > max_score:
                        max_score = current_score
                else:
                    current_score = 0

                return (current_score, max_score)

        return aux_consecutive(self)[1]

    def get_consecutive_inserts(self):
        return self.get_consecutive_indels(0)

    def get_consecutive_deletes(self):
        return self.get_consecutive_indels(1)




def node_mapping(node, path, ix_from):
    for n in path:
        if n[ix_from] == node:
            return [True, n[1-ix_from]]
    return [False, None]

def update_optimistic_score(et_node, n, score_update):
    """
    Goes up the tree until a node representing mapping of n is encountered. Its score is then
    updated and the change is propagated to all its descendants
    :param et_node:
    :param nb0:
    :param neighbor_score_update:
    :return:
    """

    while et_node.get_operation()[0] != n:
        et_node  = et_node.get_parent()

    propagate_optimistic_score_update(et_node, score_update)


def propagate_optimistic_score_update(et_node, score_update):

    et_node.node_score_optimistic += score_update
    et_node.path_score_optimistic += score_update

    for child in et_node.get_succs():
        propagate_optimistic_score_update(child, score_update)

def get_node_repr(node):
    return "[{}-{}]->map-len:{}->node-score:{}->node-score-opt:{}->mapp-score:{}->mapp-score-opt:{}".format(node.operation[0],
                                                                                      node.operation[1],
                                                                                      len(node.get_mapping().get_mapping()),
                                                                                      node.get_node_score(),
                                                                                      node.get_node_score_optimistic(),
                                                                                      node.get_mapping_score(),
                                                                                      node.get_mapping_score_optimistic())


def get_operation_cost(operation, costs):

    if not operation[0] and not operation[1]:
        return 0

    if not operation[0]:
        return costs.insert(operation)

    if not operation[1]:
        return costs.delete(operation)

    return costs.update(operation)

def op_compatibility(op, mapping_dicts):
    if (op[0] in mapping_dicts[0] and mapping_dicts[0][op[0]] != op[1]) or (op[1] in mapping_dicts[1] and mapping_dicts[1][op[1]] != op[0]):
            return False
    return True


def node_compatibility(n1, n2):
    """
    Checks whether two nodes, representing two paths in the exploration graph, can be merged.
    This is possible only if the already mapped nodes in the two root-to-node paths share mapping.
    :param n1:
    :param n2:
    :return:
    """

    n1_dicts = n1.get_mapping().get_dicts()
    n2_dicts = n2.get_mapping().get_dicts()

    return len(n1_dicts[0].items() & n2_dicts[0].items()) == len(n1_dicts[0].keys() & n2_dicts[0].keys()) and \
     len(n1_dicts[1].items() & n2_dicts[1].items()) == len(n1_dicts[1].keys() & n2_dicts[1].keys())



    # for op1 in n1.get_mapping().get_mapping():
    #     if not op_compatibility(op1, n2_dicts):
    #         # assert(test == False)
    #         # if not(test==False):
    #         #     print(str(n1.get_mapping()))
    #         #     print(str(n2.get_mapping()))
    #         #     exit()
    #         return False
    #
    # # assert (test == True)
    # return True


def get_extended_compatible_mappings(n1, n2):
    """
    In situations when merging n1 with n2 (n1 the new node) it can happen that n2 is not a leaf node and
    thus n2 does not represent the largest partil mapping which could potentially be merged. The reason
    for this is that after the operation was added to tracked operations, the mapping was extended. This
    function starts with n2 node which represent compattible mapping and explores descendants, identifying
    longest path (largest mapping) which is still compatible.
    :param n1: Node representing mapping being extended by this node.
    :param n2: Node representing existing mapping which n1 will connect to (merge)
    :return:
    """

    def rec(n, n_pred, n_tgt, mapping_dicts):
        """

        :param n:
        :param n_pred: Predecessor from which we came to n, i.e. for which we know it is compatible with n_tgt
        :param n_tgt:  The node for which we are seraching for compatible mappings
        :param mapping_dicts:
        :return: List of longest compatible mappings going through n
        """

        if not op_compatibility(n.get_operation(), mapping_dicts):
            return []
        if n_pred is not None:
            for p in n.get_preds():
                if id(p) != id(n_pred) and not node_compatibility(n_tgt, p):
                    return []


        compatible = []

        for ch in n.get_succs():
            compatible += rec(ch, n, n_tgt, mapping_dicts)

        return compatible if len(compatible) > 0 else [n]

    return rec(n2, None, n1,  n1.get_mapping().get_dicts())


def get_merge_cost(n1, n2):
    
    score_pessimistic = 0
    score_optimistic = 0

    m = [n1.get_mapping(), n2.get_mapping()]

    mn0 = [set(m[0].get_list(0)), set(m[1].get_list(0))] #mapped nodes in G1

    mn0_intersection = mn0[0].intersection(mn0[1])

    edges = [ExplorationGraph.graphs[0].get_graph().edges,ExplorationGraph.graphs[1].get_graph().edges]
    md0 = merge_two_dicts(m[0].get_dict(0), m[1].get_dict(0))

    # score for mathing nodes common in both sets would be included twice after merging
    for n in mn0_intersection :
        # score = ExplorationGraph.node_costs.update(
        #     [n, md0[n]])  # does no matter if md[0] or md[1] since the mappings should be compatible
        if n is not None:
            if md0[n] is not None:
                score = ExplorationGraph.node_costs.update(
                    [n, md0[n]])  # does no matter if md[0] or md[1] since the mappings should be compatible
            else:
                score = ExplorationGraph.node_costs.delete(n)
        else:
            #find all nodes which have been inserted and check if they have been inserted twice
            for n_ins in set(m[0].get_inserted(1)).intersection(m[1].get_inserted(1)):
                score = ExplorationGraph.node_costs.insert(n_ins)

        score_pessimistic -= score
        score_optimistic -= score

    ####### Check how edges in G1 between the considered mappings are mapped in G2 (possible edge deletion)

    for (u,v) in nx.edge_boundary(ExplorationGraph.graphs[0].get_graph(), mn0[0], mn0[1]):
        # take all pairs of edges (n1,n2) where n2 is in one mapping, but not the other (n1 can be shared)
        # check whether f(n1), f(n2) are connected as well
        # if so, the cost of mapping n1 and n2 need to be decreased because when we mapped n1 and n2 we, by default,
        # considered the pesimistic case that is that the edge between n1 and n2 does not exist in the target graphs
        # and so now, when we found out that given edge was preserved after the mapping, this cost needs to be deduced
        if u in mn0_intersection and v in mn0_intersection:
            # for some reason the edge boundary also returns edges in the intersection
            #TODO !!!!! maybe replace with intersection
            if (md0[u], md0[v]) not in edges[1]:
                # if an edge has not been mapped then cost for that is in both n1 and n2 and so needs to be
                # deduced from the sum otherwise it would be included twice
                score = ExplorationGraph.edge_costs.delete([u, v])
                score_pessimistic -= score
                score_optimistic -= score
        else:
            if (md0[u], md0[v]) in edges[1]:
                # "times" counts how many times the cost for edge deletion has been added to the pessimistic score and not deducted.
                # If the edge is included in one of the mappings then it has already been deducted and should not be
                # done so again in during the merging
                times = 2
                for i in range(2):
                    if u in mn0[i] and v in mn0[i]:
                        times -= 1
                score_pessimistic -= times * ExplorationGraph.edge_costs.delete([u, v])
            else:
                if not (u in mn0[0] and v in mn0[0]) and not (u in mn0[1] and v in mn0[1]):
                    # if the both ends of the considered edge are already in one of the mappings then
                    # it has been already included in the optimistic score of the corresponding mapping and should
                    # not be included second time
                    score_optimistic += ExplorationGraph.edge_costs.delete([u, v])
                elif u in mn0_intersection or v in mn0_intersection:
                    # cost of the removed edge was included in the pessimitic score of both of the mappings because
                    # this edge has one node which is in both mappings
                    score_pessimistic -= ExplorationGraph.edge_costs.delete([u, v])
                    if (u in mn0_intersection and md0[u] is None) or (v in mn0_intersection and md0[v] is None):
                        #if the node on the border was removed then in both of the mappings all edges were removed
                        #and penalty included also in optimistic score variants
                        score_optimistic -= ExplorationGraph.edge_costs.delete([u, v])

    ####### Check how edges in G2 between the considered mappings are mapped in G1 (possible edge insertion)

    mn1 = [set(m[0].get_list(1)), set(m[1].get_list(1))]  # images of mapped nodes in G2
    md1 = merge_two_dicts(m[0].get_dict(1), m[1].get_dict(1))
    mn1_intersection = mn1[0].intersection(mn1[1])

    for (u, v) in nx.edge_boundary(ExplorationGraph.graphs[1].get_graph(), mn1[0], mn1[1]):
        if u in mn1_intersection  and v in mn1_intersection:
            # for some reason the edge boundary also returns edges in the intersection
            if (md1[u], md1[v]) not in edges[0]:
                # if an edge has been added then cost for that is in both n1 and n2 and so needs to be
                # deduced from the sum otherwise it would be included twice
                s = ExplorationGraph.edge_costs.insert([u, v])
                score_pessimistic -= s
                score_optimistic -= s
        else:
            if (md1[u], md1[v]) not in edges[0]:
                if not(md1[u] in mn0[0] and md1[v] in mn0[0]) and not(md1[u] in mn0[1] and md1[v] in mn0[1]):
                    # If the edge in G2 is not in G1 and its nodes are not in both n1 and n2 mappings =>
                    # the cost for edge insertion has already been included and should not be counted again
                    s = ExplorationGraph.edge_costs.insert([md1[u], md1[v]])
                    score_optimistic += s
                    score_pessimistic += s

    return {
        'optimistic': n1.get_mapping_score_optimistic() + n2.get_mapping_score_optimistic() + score_optimistic,
        'pessimistic': n1.get_mapping_score() + n2.get_mapping_score() + score_pessimistic
    }
