
class Costs():

    def __init__(self, insert=None, update=None, delete=None):

        if insert is None:
            insert = lambda x: 1
        if delete is None:
            delete = lambda x: 1
        if update is None:
            update = lambda x: 0

        self.costs = {
            "insert": insert,
            "update": update,
            "delete": delete
        }

    def insert(self, operation):
        return self.costs["insert"](operation)

    def update(self, operation):
        return self.costs["update"](operation)

    def delete(self, operation):
        return self.costs["delete"](operation)

    def cost(self, op):
        if op[0] is None:
            return self.insert(op)
        if op[1] is None:
            return self.delete(op)
        else:
            return self.update(op)

