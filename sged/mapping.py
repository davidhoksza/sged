class Mapping(object):

    def __repr__(self):
        return "-".join(['{}:{}'.format(op[0], op[1]) for op in self.get_mapping()])

    def serialize(self):
        if self.cache is None:
            m = self.get_mapping()

            m.sort(key=lambda x: "" if not x else x)

            self.cache = "-".join(['{}:{}'.format(op[0], op[1]) for op in m])
        return self.cache

    def exists_in_mappings(self, mappings):
        # s = self.serialize()
        # for m in mappings:
        #     if s in m.serialize():
        #         return True
        # return False
        for m in mappings:
            if self.exists_in_mapping(m):
                return True
        return False

    def exists_in_mapping(self, mapping):
        #the condition on length is important, because the mappings can be identical but
        #None can map to multiple nodes (deletions) and without this condition a
        #mapping with many deletions could be considered as a subset of mapping with less/different deletions
        return self.get_set(0).issubset(mapping.get_set(0)) and len(self) <= len(mapping)

    def __str__(self):
        return self.__repr__()

    def __init__(self, _mapping_list):

        self.cache = None

        self.mapping_sets = [set(), set()]
        self.mapping_lists = [[], []]
        self.mapping_dicts = [{}, {}]
        self.inserted = [[], []]
        self.mapping_lists_upd = [[], []]
        self.mapping_list_upd = []
        self.mapping_list_ins = []
        self.mapping_list_del = []

        # if len(_mapping_list) == 1 and not _mapping_list[0].is_valid():
        #     #     mapping for the root where the only operation is [None;None]
        #     self.mapping_list = []
        #     return

        for m in _mapping_list:
            self.mapping_sets[0].add(m[0])
            if m[0] is not None:
                self.mapping_lists[0].append(m[0])
                self.mapping_dicts[0][m[0]] = m[1]
                if m[1] is None:
                    self.inserted[0].append(m[0])
                    self.mapping_list_ins.append(m)
                else:
                    self.mapping_list_upd.append(m)
                    self.mapping_lists_upd[0].append(m[0])
                    self.mapping_lists_upd[1].append(m[1])

            self.mapping_sets[1].add(m[1])
            if m[1] is not None:
                self.mapping_lists[1].append(m[1])
                self.mapping_dicts[1][m[1]] = m[0]
                if m[0] is None:
                    self.inserted[1].append(m[1])
                    self.mapping_list_del.append(m)

        self.mapping_list = _mapping_list
        self.mapping_list_upd = sorted(self.mapping_list_upd)
        self.mapping_lists_upd = [sorted(self.mapping_lists_upd[0]), sorted(self.mapping_lists_upd[1])]
        self.inserted = [sorted(self.inserted[0]), sorted(self.inserted[1])]
        self.mapping_list_ins = sorted(self.mapping_list_ins)
        self.mapping_list_del = sorted(self.mapping_list_del)

    def __len__(self):
        return len(self.get_mapping())

    def get_dict(self, ix):
        return self.mapping_dicts[ix]

    def get_dicts(self):
        return self.mapping_dicts

    def get_list(self, ix):
        return self.mapping_lists[ix]

    def get_list_upd(self, ix):
        return self.mapping_lists_upd[ix]

    def get_set(self, ix):
        return self.mapping_sets[ix]

    def get_mapped(self, ix):
        mapped = []
        for n in self.get_list(ix):
            if n is not None:
                mapped.append(n)

        return mapped

    def get_inserted(self, ix=None):
        return self.inserted if ix is None else self.inserted[ix]

    def get_mapping(self):
        return self.mapping_list

    def get_mapping_upd(self):
        return self.mapping_list_upd

    def get_mapping_ins(self):
        return self.mapping_list_ins

    def get_mapping_del(self):
        return self.mapping_list_del

    def get_mapping_inserted(self):
        return self.mapping_list_ins

    def get_list_of_lists(self):
        rv = []
        for m in self.get_mapping():
            rv.append([m[0], m[1]])
        return rv