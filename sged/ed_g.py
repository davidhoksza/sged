import networkx as nx
import sys
import copy
import random
import time

# from sged.exploration_tree import ExplorationTree
from .exploration_graph import ExplorationGraph
from .exploration_graph import initExplorationGraph
from .cost import Costs
from .operation import Operation

from .open_set import OpenSet

settings = {
    'max_open_set_category_size': -1 #maximum number of nodes in open set per mapping size, i.e. open set can contain at most given number of nodes with given mapping size
    
    , 'max_children': 2
    , "log_to_console": False
    , "penalize_superfluous_edges": True    # penalty for edges (v^2_1, v^2_2) \in E(G2) such that
                                            # there exists v^1_1, v^1_2: f(v^1_1)=v^2_1 && f(v^1_2)=v^2_2 && (v^1_1, v^1_2) not \in E(G1)
    , "max_consecutive_inserts": 2
    , 'use_adjecency_matrix_scoring': True

    ,'seed_pairs_count': 100
    ,'max_open_set_category_sizes': []

    , 'switch_if_first_bigger': False

    , 'max_time_in_s': -1
    , 'must_find_solution': False
    # , "max_consecutive_deletes": -1
}

graph_cache = []



def test_with_ops(ops, eg):

    nodes =[eg]
    for op in ops:
        new_nodes = []
        for node in nodes:
            new_nodes += node.add_operation(op)
        nodes = new_nodes

    min_mapping = {'len': 0, 'mapping': [], 'distance': sys.float_info.max}

    for n in nodes:
        l = len(n.get_mapping().get_list(0))
        d = n.get_mapping_score()
        if  l > min_mapping['len'] or (l == min_mapping['len'] and d > min_mapping['distance']):
            min_mapping = {'len': l, 'mapping': n.get_mapping().get_list_of_lists(), 'distance': d}

    return [min_mapping]


def switch_graphs(g1, g2, node_costs, edge_costs):

    # def switch_node_op(op):
    #     if isinstance(op, list) or isinstance(op, Operation):
    #         return node_costs.update([op[1], op[0]])
    #     else:
    #         return node_costs.update(op)

    if settings['switch_if_first_bigger'] and len(g1) > len(g2):

        if settings["log_to_console"]: print("Switching role of g1 and g2")

        node_costs_s = Costs(insert=node_costs.delete,
                             delete=node_costs.insert,
                             update=lambda op: node_costs.update([op[1], op[0]]))
        edge_costs_s = Costs(insert=edge_costs.delete,
                             delete=edge_costs.insert,
                             update=lambda op: edge_costs.update([op[1], op[0]]))

        return True, g2, g1, node_costs_s, edge_costs_s
    else:
        return False, g1, g2, node_costs, edge_costs



def switch_mappings(switched, mappings, g1, g2, node_costs, edge_costs):
    if not switched:
        return mappings


    for md in mappings:
        m = md['mapping']
        d = md['distance']

        mapped = []
        for i in range(len(m)):
            m[i] = [m[i][1], m[i][0]]
            mapped.append(m[i][0])

        for n in g2:
            if n not in mapped:
                m.append([n, None])
                d += node_costs.insert(n)
                for nb in nx.all_neighbors(g2, n):
                    d += edge_costs.insert([n,nb])

        md['distance'] = d

    return mappings


def update_settings(user_settings):

    settings.update(user_settings)
    if len(settings['max_open_set_category_sizes']) == 0:
        if (settings['max_open_set_category_size'] > 0):
            settings['max_open_set_category_sizes'].append(settings['max_open_set_category_size'])

def sged(g1, g2, node_costs=Costs(), edge_costs=Costs(), user_settings={}, test_ops = []):
    """

    :param g1:
    :param g2:
    :param node_costs:
    :param edge_costs:
    :param user_settings:
    :param test_ops:
    :param neighbor_preferences: Array of two matrices, each of which defines how preferred is for
    :return:
    """

    update_settings(user_settings)

    [switched, sg1, sg2, node_costs_s, edge_costs_s] = switch_graphs(g1, g2, node_costs, edge_costs)
    mappings = sged_switched(sg1, sg2, switched, node_costs_s, edge_costs_s, test_ops)
    return switch_mappings(switched, mappings, sg1, sg2, node_costs_s, edge_costs_s)
    # return mappings


def sged_switched(g1, g2, switched, node_costs=Costs(), edge_costs=Costs(), test_ops = []):

    time_start = time.time()

    initExplorationGraph()

    # graphs = first_smaller(g1, g2)
    graphs = [g1, g2]

    open_set = OpenSet()
    touched_paths = set()

    # eg_start = ExplorationTree(graphs, node_costs=node_costs, edge_costs=edge_costs, settings=settings)
    eg_start = ExplorationGraph(graphs, node_costs=node_costs, edge_costs=edge_costs, settings=settings)

    if (len(test_ops) > 0):
        return test_with_ops(test_ops, eg_start)

    nodes = [sorted(list(graphs[0].nodes)), sorted(list(graphs[1].nodes))]
    # node1 = sorted(pick_seed_nodes(nodes, cnt=1, node_costs=node_costs))[0]
    # if settings["log_to_console"]:
    #     if settings["log_to_console"]: print("Starting with node: {}".format(node1))
    # for node2 in nodes[1] + [None]:
    #     if node1 or node2:
    #         open_set.add(eg_start.add_operation([node1, node2]))
            # add_to_open_set(open_set, eg_start.add_operation([node1, node2]))

    for (n1, n2, c) in pick_seed_node_pairs(nodes, cnt=settings['seed_pairs_count'], node_costs=node_costs):
        open_set.add(eg_start.add_operation([n1, n2]))

    min_mappings = []
    cnt_tested = 0
    while len(open_set) > 0:

        time_elapsed = time.time() - time_start

        if (len(min_mappings) > 0 and (len(open_set) == 0 or open_set.min_solution().get_mapping_score_optimistic() > min_mappings[0]["distance"])) \
                or (settings['max_time_in_s'] >= 0 and time_elapsed > settings['max_time_in_s'] and (not settings['must_find_solution'] or len(min_mappings) > 0)) :
            if settings["log_to_console"]: print("Tested solutions: {}".format(cnt_tested))
            return min_mappings

        if cnt_tested % 100 == 0:
            if settings["log_to_console"]: print("{}s, Tested: {}, OS size: {}, ET size: {}, min dist: {}".format(
                time_elapsed,
                cnt_tested,
                len(open_set),
                len(eg_start),
                min_mappings[0]["distance"] if len(min_mappings) > 0 else ""))

        eg = open_set.pop()

        if settings["log_to_console"]:
            print("{}s: Tested solutions: {} (eg:{}, mapping:{}, min dist: {}, eg size: {})".format(
                time_elapsed,
                cnt_tested,
                eg,
                eg.get_mapping(),
                min_mappings[0]["distance"] if len(min_mappings) > 0 else "",
                len(eg_start)))

        # if len(min_mappings) > 0 and eg.get_mapping_score_optimistic() > min_mappings[0]["distance"]:
        if len(min_mappings) > 0 and eg.get_mapping_score_optimistic() > min_mappings[0]["distance"]:
            continue

        cnt_tested += 1

        # TODO - define possibility to specify which or whether this should be semi-global alignment
        # get list of already mapped, inserted, and deleted nodes of the current partial solution
        mapped = eg.get_mapping().get_list(0)

        # if the number of mapped nodes in g1 equals the number of all nodes in g1, the solution is complete
        # TODO !!!!!!!!!!!!!!!!!!!! DO NOT STORE FINISHED MAPPINGS INTO THE QUEUE
        if len(mapped) == len(graphs[0].nodes()):

            # assert eg.get_mapping_score() == eg.get_mapping_score_optimistic()

            new_score = eg.get_mapping_score()

            if len(min_mappings) == 0 or new_score <= min_mappings[0]["distance"]:
                if len(min_mappings) > 0 and new_score < min_mappings[0]["distance"]:
                    min_mappings.clear()
                min_mappings.append({
                    "mapping": eg.get_mapping().get_list_of_lists(),
                    "distance": new_score
                })
                if settings["log_to_console"]:
                    if settings["log_to_console"]: print("New mapping:{}".format(eg))

            continue

        #extend mapping

        open_set.add(extend_mapping(eg, graphs, touched_paths, node_costs, switched))

        open_set.prune(settings["max_open_set_category_sizes"])

    return min_mappings


def extend_mapping(eg, graphs, touched_paths, node_costs, switched):
    mapping = eg.get_mapping()
    mapped = [mapping.get_list(0), mapping.get_list(1)]

    nodes_to_add = []
    tested_ops = []
    for i in range(2):
        v1_candidates = {}
        eb = list(nx.edge_boundary(graphs[i], mapped[i]))
        eb.sort()  # sorting so that the result is deterministic for the purpose of debugging
        for (v1, v2) in eb:

            if v1 not in v1_candidates:
                candidates = get_candidate_nodes(v1, graphs[i], graphs[1 - i], mapped[i], mapped[1 - i],
                                                        mapping.get_dict(i), mapping.get_dict(1-i))

                # v1_candidates[v1]  =candidates
                v1_candidates[v1] = [c for c in candidates if c not in mapped[1-i]]

                # if len(v1_candidates[v1]) == 0:
                #     v1_candidates[v1] = graphs[1 - i].nodes()

            nodes2 = sorted(v1_candidates[v1]) #sorting to make the computation deterministic

            for node2 in list(nodes2) + [None]:

                # if node2 not in mapped[1 - i]:
                    if i == 0:
                        op = [v2, node2]
                    else:
                        op = [node2, v2]

                    if op in tested_ops:
                        # This operation might have been already tried in current round of extension (e.g. when
                        # extending partial identity mapping of identical graph - then i == 1 and i == 2 can have
                        # some identical operations)
                        continue
                    else:
                        tested_ops.append(op)

    ops = sorted(tested_ops, key=lambda x:node_costs.cost(x))

    if settings['max_children'] > 0:
        ix_max = settings['max_children']

        encountered_g1_extension = False
        for i in range(min(len(ops), ix_max)):
            if ops[i][0] is not None:
                encountered_g1_extension = True

        while ix_max < len(ops) and (
                node_costs.cost(ops[ix_max]) == node_costs.cost(ops[ix_max-1]) or not encountered_g1_extension
        ):
            if ops[ix_max][0] is not None:
                encountered_g1_extension = True # ensures that a solution will be found fast, otherwise a path to a first solution could be filtered out
            ix_max += 1
            
        ops = ops[:ix_max]


    for op in ops:
        for new_node in eg.add_operation(op):

            s_mapp = new_node.get_mapping().serialize()
            if s_mapp not in touched_paths:
                touched_paths.add(s_mapp)

                # cnt_consecutive = new_node.get_consecutive_inserts() if not switched else eg.get_consecutive_deletes()
                cnt_consecutive = new_node.get_consecutive_inserts() # TODO this can lead to more consecutive inserts when switched, but without it it can be problematic to find a solution at all
                if settings["max_consecutive_inserts"] >= 0 and cnt_consecutive > settings["max_consecutive_inserts"]:
                    continue

                nodes_to_add.append(new_node)

    return nodes_to_add

def get_not_mapped_neighbors_indirect(n, g, map_dict):
    """
    Returns all neibhors including indirect ones. Indirect neighbor with respect to existing mapping
    is connected to n by a path where all nodes on the path are mapped to None, i.e. deleted.
    :param n:
    :param map_dict:
    :return:
    """

    q = list(nx.all_neighbors(g, n))
    nbs = []
    processed = set()
    while len(q) > 0:
        n1 = q.pop()
        processed.add(n1)
        if n1 not in nbs:
            if n1 not in map_dict:
                nbs.append(n1)
            elif map_dict[n1] is None:
                q += [x for x in list(nx.all_neighbors(g, n1)) if x not in processed]

    return nbs


def get_candidate_nodes(n, g1, g2, mapped1, mapped2, map1, map2):
    """
    Returns nodes in g2 onto which any neighbor of n could potentially be mapped with respect to existing mapping.
    By neighbor we mean here not only direct neighbors in g1 but also neighbors after removal of existing neighbors.
    Eg. in graph A-B-C, if n=A and B is mapped to None then C becomes a neighbor of A
    :param n:
    :param g1:
    :param g2:
    :param mapped1:
    :param mapped2:
    :param map1:
    :return:
    """
    candidates2 = []
    q = [n]
    processed = set()
    while len(q) > 0:
        n1 = q.pop()
        processed.add(n1)
        if map1[n1] is not None:
            # for nb2 in nx.all_neighbors(g2, map1[n1]):
            #         candidates2.add(nb2)
            candidates2 += get_not_mapped_neighbors_indirect(map1[n1], g2, map2)
        else:
            for nb in nx.all_neighbors(g1, n1):
                if nb not in processed and nb in mapped1:
                    q.append(nb)

    return set(candidates2)


def remove_current_from_os(open_set):
    del open_set[0]

def prune_os(open_set):
    if settings["max_open_set_category_size"] > 0:
        threshold = settings["max_open_set_size"]
        # min_score = open_set_sort_key(open_set[0])
        # while open_set_sort_key(open_set[threshold]) <= min_score:
        #     threshold += 1
        del open_set[threshold:]



def pick_seed_nodes(nodes, cnt = 1, node_costs=Costs()):
    costs = []
    for n1 in nodes[0]:
        for n2 in nodes[1]:
            costs.append([n1, n2, node_costs.update([n1, n2])])
    costs.sort(key=lambda x: x[2])

    seeds = []
    for i in range(len(costs)):
        if costs[i][0] not in seeds:
            seeds.append(costs[i][0])
        if len(seeds) == cnt:
            break

    return seeds


def pick_seed_node_pairs(nodes, cnt = 1, node_costs=Costs()):
    costs = []
    for n1 in nodes[0]:
        for n2 in nodes[1]:
            costs.append([n1, n2, node_costs.update([n1, n2])])
    costs.sort(key=lambda x: x[2])

    ix = cnt
    while ix < len(costs)-1 and costs[ix][2] == costs[ix+1][2]:
        ix += 1

    return costs[:ix]


# def pick_min_scoring_path(open_set):
#     return 0
#     min_score = sys.float_info.max
#     min_ix = -1
#     min_depth = -1
#     for ix in range(len(open_set)):
#         # score = open_set[ix].get_mapping_score() + open_set[ix].get_expected_path_score()
#         score = open_set[ix]["expected score"]
#         depth = open_set[ix]["node"].get_depth()
#         if score < min_score or (score == min_score and depth > min_depth):
#             min_score=score
#             min_ix = ix
#             min_depth = depth
#
#     return min_ix


def add_to_open_set(os, nodes):
    os += nodes


def serialize_operation(op):
    # TODO make sure that two operations can't have the same code (eg. a-|ab vs a|-ab)
    return (op[0] if op[0] else 'NONE') + "-" + (op[1] if op[1] else 'NONE')


def serialize_path(path):
    s_path = [serialize_operation(op) for op in path]
    s_path.sort()
    return "_".join(s_path)


def path_and_op_touched(paths, path, op):
    new_s_path = serialize_path(path + [op])
    return new_s_path in paths


def add_touched_paths(paths, path):
    s_path = serialize_path(path)
    # ixs_to_remove = []
    # for ix in range(len(paths)):
    #     if s_path.find(paths[ix]) >=0:
    #         ixs_to_remove.append(ix)
    # for ix in ixs_to_remove[::-1]:
    #     del paths[ix]
    paths.add(s_path)
