class Operation(object):

    def __repr__(self):
        return "-".join([self.op[0] if self.op[0] else '', self.op[1] if self.op[1] else '']) #TODO fix issues with separator apeearing in the node id

    def __init__(self, op):
        self.op = op
        self.opNone = [x if x is not None else "" for x in op]

    def __getitem__(self, ix):
        return self.op[ix] #if ix < len(self.op) else None

    def __lt__(self, other):
        return self.opNone < other.opNone

    def __hash__(self):
        return hash(str(self))

    def __eq__(self, other):
        return self.op[0] == other.op[0] and self.op[1] == other.op[1]

    def is_valid(self):
        return self.op[0] is not None or self.op[1] is not None