import sys
from sortedcontainers import SortedListWithKey


def open_set_sort_key(x):
    return x.get_expected_optimistic_score() #+ random.random() / 10


class OpenSet(object):

    def __init__(self):
        self.os_cat = {} # open set divided into categories by mapping size of the partial solutions
        self.last_picked_cat_key = -1
        self.cnt_category_picked = 0 # used to control how many times

    def __len__(self):
        return sum([len(v) for k, v in self.os_cat.items()])


    def add(self, nodes):

        if not isinstance(nodes, list):
            nodes = [nodes]

        for n in nodes:
            l = len(n.get_mapping())
            if l not in self.os_cat:
                self.os_cat[l] = SortedListWithKey(key=open_set_sort_key)

            self.os_cat[l].add(n)

    def min(self):
        return min([v.key(v[0]) for k, v in self.os_cat.items()])

    def min_solution(self):

        min_solution = {
            'dist': sys.float_info.max
            , 'node': None
        }

        for k, cat in self.os_cat.items():
            if len(cat) > 0:
                eos = cat[0].get_expected_optimistic_score()
                if eos < min_solution['dist']:
                    min_solution = {
                        'dist': eos
                        , 'node': cat[0]
                    }

        return min_solution['node']

    def pop(self):

        os_keys_sort = sorted(self.os_cat.keys())
        if len(os_keys_sort) == 0 or len(self.os_cat) == 0:
            return None

        if self.last_picked_cat_key > 0:
            ix = os_keys_sort.index(self.last_picked_cat_key)
            if len(self.os_cat[os_keys_sort[ix]]) > 0 and self.cnt_category_picked < 0:
                self.cnt_category_picked +=1
            else:
                while True:
                    ix = (ix + 1) % len(os_keys_sort)
                    if len(self.os_cat[os_keys_sort[ix]]) > 0:
                        self.cnt_category_picked = 0
                        break

            self.last_picked_cat_key = os_keys_sort[ix]
        else:
            self.last_picked_cat_key = os_keys_sort[0]

        return self.os_cat[self.last_picked_cat_key].pop(0)

        # following code is to make the code more deterministic by picking the same object when the keys are the same
        # candidates = [self.os_cat[self.last_picked_cat_key][0]]
        # i = 1
        # while i < len(self.os_cat[self.last_picked_cat_key]) and self.os_cat[self.last_picked_cat_key][i] == candidates[0]:
        #     candidates.append(self.os_cat[self.last_picked_cat_key][i])
        #     i+=1
        # candidates.sort(key=lambda x:x.get_mapping().serialize())
        # self.os_cat[self.last_picked_cat_key].remove(candidates[0])
        # return candidates[0]



        min_solution = {
            'dist': sys.float_info.max
            , 'node': None
        }

        for k, cat in self.os_cat.items():
            if len(cat) > 0:
                eos = cat[0].get_expected_optimistic_score()
                if eos < min_solution['dist']:
                    min_solution = {
                        'dist': eos
                        , 'node': cat[0]
                    }

        if min_solution['node'] is not None:
            l = len(min_solution['node'].get_mapping())
            del self.os_cat[l][0]

        return min_solution['node']

    def prune(self, max_category_sizes):
        """
        Pruning each category (bin) to keep at most max_category_sizes values. If there are more
        items with the same value at the end, all of them are kept. I.e., it is possible that more
        items will be kept then given by the max_category_sizes parameter.
        :param max_category_sizes: Array of intended categories sizes. If empty, no pruning happens. If there
        are less values in the array than there are categories, the last value is used for the remaining
        categories. I.e., if the values are [100.10] and there are 4 categories. the last three categories
        will have size 10.
        :return:
        """

        if len(max_category_sizes) == 0:
            return

        for k, cat in self.os_cat.items():

            ix = k - 2 #k-2 because mapping containes None:None for root and mapping of size one is at position 0
            max_category_size = max_category_sizes[ix] if ix < len(max_category_sizes) else max_category_sizes[-1]

            if len(cat) > max_category_size:
                # Iteration to move the index to the position where the key based on which the values
                # are sorted differs. Otherwise the pruning is not deterministic since there is no way
                # to tell SortedContainer to keep the order of the items with the same key
                # when running over the same data.
                ix_max = max_category_size
                # print("ix_max {}".format(ix_max))
                # print("cat {}".format(cat))
                while ix_max < len(cat) and cat.key(cat[ix_max]) == cat.key(cat[ix_max-1]):
                    ix_max += 1
                if (ix_max < len(cat)):
                    del cat[ix_max:]

                # del cat[max_category_size:]