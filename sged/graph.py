import networkx as nx


class Graph(object):

    def __init__(self, graph):
        self.graph = graph

        self.nodes = sorted(list(graph.nodes()))

        self.node_ix_mapping = {}
        for i in range(len(self.nodes)):
            self.node_ix_mapping[self.nodes[i]] = i

        self.adj_matrix = nx.convert_matrix.to_numpy_matrix(graph, nodelist=self.nodes)

    def get_graph(self):
        return self.graph

    def get_adj_matrix(self):
        return self.adj_matrix

    def get_node_ix(self, node):
        return self.node_ix_mapping[node]

