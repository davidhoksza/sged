import networkx as nx
from .cost import Costs


class ExplorationTree(object):

    graphs = [] #static shared by
    use_cache = True
    settings = {}

    def __init__(self, graphs=graphs, operation=[None,None], node_costs=Costs(), edge_costs=Costs(), settings={}):
        if len(settings):
            ExplorationTree.settings = settings

        self.set_operation(operation)
        if graphs:
            if len(graphs) == 2:
                ExplorationTree.graphs = graphs
            else:
                raise Exception('Wrong number of graphs passed to the ExplorationTree object')
        self.children = []
        self.node_costs = node_costs
        self.edge_costs = edge_costs
        self.parent = None
        # node_score gets the score of a node considering the worst possible scenario,
        # i.e. not yet mapped neibhors of a node are treated as nodes which will be mapped to a non-neighbor
        # and only when it turns out that two neighboring nodes stay neighbors in the target graph the
        # score will be corrected
        self.node_score = 0
        # node_score_optimistic considers the best possible case, i.e. ale neighbors of a mapped node are
        # considered to be neighbors of the same node even after mapping and only when all neibhors of a node
        # are mapped, this will be corrected
        self.node_score_optimistic = 0
        self.path_score = 0
        self.path_score_optimistic = 0
        self.set_depth(0)
        self.cache = {} # Storing once computed stuff such as path

    def __repr__(self):
        return get_node_repr(self)

    def __str__(self):
        return get_node_repr(self)

    def __len__(self):
        return sum([len(ch) for ch in self.children]) if len(self.children) > 0 else 1


    def set_operation(self, operation):
        self.operation = operation

    def add_child(self, node):
        assert isinstance(node, ExplorationTree)
        node.set_parent(self)
        self.children.append(node)

    def get_children(self):
        return self.children

    def get_last_processed(self):
        last = [None,None]
        node = self
        while not last[0] and not last[1] and node:
            for i in range(2):
                if not last[i] and node.operation[i]:
                    last[i] = node.operation[i]
            node = node.get_parent()
        return last

    def get_processed(self):
        """
        Get list of already mapped, inserted or deleted nodes
        :return:
        """
        node = self
        processed = [[], []]
        while node:
            if node.operation[0]:
                processed[0].append(node.operation[0])
            if node.operation[1]:
                processed[1].append(node.operation[1])
            node = node.get_parent()
        return processed

    def add_operation_child(self, operation):
        node = ExplorationTree(operation=operation, node_costs=self.get_node_costs(), edge_costs=self.get_edge_costs())
        self.add_child(node)
        node.set_scores()
        return node

    def set_parent(self, node):
        self.parent = node
        self.set_depth(node.get_depth() + 1)

    def get_parent(self):
        return self.parent

    def set_depth(self, depth):
        self.depth = depth

    def get_depth(self):
        return self.depth

    def get_node_costs(self):
        return self.node_costs

    def set_node_costs(self, costs):
        self.node_costs = costs

    def get_edge_costs(self):
        return self.edge_costs

    def set_edge_costs(self, costs):
        self._costs = costs

    def get_node_score(self):
        return self.node_score

    def get_node_score_optimistic(self):
        return self.node_score_optimistic

    def get_path_score(self):
        return self.path_score

    def get_path_score_optimistic(self):
        return self.path_score_optimistic

    def get_expected_path_score(self):
        return 0

    def set_scores(self):

        if self.operation[0] or self.operation[1]:

            if not self.operation[0]:
                # adding a node

                # cost for adding the node
                self.node_score += self.node_costs.insert(self.operation)
                self.node_score_optimistic += self.node_costs.insert(self.operation)

                # cost for adding edge between the node and all already mapped nodes
                path = self.get_path(as_two_lists=False)
                for nb1 in nx.all_neighbors(ExplorationTree.graphs[1], self.operation[1]):
                    for [n0,n1] in path:
                        if n1 == nb1 and n0:
                            self.node_score += self.edge_costs.insert([self.operation[1], n1])
                            break

            elif not self.operation[1]:
                # deleting a node

                # cost for removing a node
                self.node_score += self.node_costs.delete(self.operation)
                self.node_score_optimistic += self.node_costs.delete(self.operation)

                # cost for removing its connecting edges
                for n in nx.all_neighbors(ExplorationTree.graphs[0], self.operation[0]):
                    self.node_score += self.edge_costs.delete((self.operation[1],n))
                    self.node_score_optimistic += self.edge_costs.delete((self.operation[1], n))

            else:
                # mapping a node X in G1 to a node Y G2

                # cost for mapping the nodes themselves (not including edges)
                self.node_score += self.node_costs.update(self.operation)
                self.node_score_optimistic += self.node_costs.update(self.operation)

                #find each neighbors N of X
                path = self.get_path(as_two_lists=False)
                for nb0 in nx.all_neighbors(ExplorationTree.graphs[0], self.operation[0]):
                    [nb0_mapped, nb1] = node_mapping(nb0, path, 0)
                    if not nb0_mapped:
                        # if N is not yet mapped, add cost for removal of edge (N,X)
                        # (this might be reverted when N will be mapped to a node f(N) such that f(N) and f(X) are neighbors in G2)
                        self.node_score += self.edge_costs.delete((self.operation[0], nb0))
                    else:
                        # if N is mapped and f(N) and f(X) are neighbors in G2 remove cost for (N,X)
                        if nb1 and nb1 in nx.all_neighbors(ExplorationTree.graphs[1], self.operation[1]):
                            # decrease the score by the cost of edge removal since this was "incorrectly" added when introducing mapping of N
                            self.node_score -= self.edge_costs.delete((self.operation[0], nb0))
                        else:
                            # if N was inserted, add cost for inserting the new edge, because when N was mapped,
                            # X was not yet mapped and thus cost for edge N-X was not added to the score for introducing X
                            self.node_score += self.edge_costs.insert((self.operation[0], nb0))

                        # update the optimistic score by finding out whether any already mapped neighbor N of the existing node X
                        # already has all of its neighbors mapped and if yes increase cost by adding penalties for neighbors NN of N
                        # which are not neighbors in G2, ie. (f(N),f(NN)) not in E(G2)
                        if nb1:
                            # if the neighbor nb0 is mapped to a node nb1 (and therefore not deleted)

                            neighbor_score_update = 0

                            all_mapped = True
                            neigbhors_mappings = []
                            for nnb0 in nx.all_neighbors(ExplorationTree.graphs[0], nb0):
                                [mapped, nnb1] = node_mapping(nnb0, path, 0)
                                neigbhors_mappings.append([nnb0, nnb1])
                                if not mapped:
                                    all_mapped = False
                                    break
                            if all_mapped:
                                # add cost for deleting edges, i.e. cost for mapping neighbors of nb0 to non-neighbors of nb1
                                for [nnb0, nnb1] in neigbhors_mappings:
                                    if not nnb1 or (nnb1 not in nx.all_neighbors(ExplorationTree.graphs[1], nb1)):
                                        # the neighbor nnb0 of nb0 was deleted
                                        # or the neighbor nnb0 was mapped to the node nnb1 which is not neighbor of nb1 in G2
                                        neighbor_score_update += self.edge_costs.delete([nb0, nnb0])

                            self.node_score_optimistic += neighbor_score_update
                            # if neighbor_score_update > 0:
                            #     update_optimistic_score(self, nb0, neighbor_score_update)

                if ExplorationTree.settings["penalize_superfluous_edges"]:
                    neighbor_score_update = 0
                    # for all already mapped neighbors nb1 of Y in G2 test whether (X,f^-1(nb1)) \in E(G1)
                    for nb1 in nx.all_neighbors(ExplorationTree.graphs[1], self.operation[1]):
                        [mapped, nb0] = node_mapping(nb1, path, 1)
                        if nb0 and nb0 not in nx.all_neighbors(ExplorationTree.graphs[0], self.operation[0]):
                            # the penalties in situation when the neighbor was inserted is already accounted for when treating node insertion
                            neighbor_score_update += self.edge_costs.insert([self.operation[1], nb1])

                    self.node_score_optimistic += neighbor_score_update
                    self.node_score += neighbor_score_update


        self.path_score = self.node_score + self.get_parent().get_path_score()
        self.path_score_optimistic = self.node_score_optimistic + self.get_parent().get_path_score_optimistic()

    def get_operation(self):
        return self.operation

    def get_path(self, full=False, as_two_lists=False):

        if ExplorationTree.use_cache:
            cache_key = "path_{}_{}".format(full, as_two_lists)
            if cache_key in self.cache:
                return self.cache[cache_key]

        node = self
        path = [[],[]] if as_two_lists else []
        while node and (full or node.parent): #root is an auxiliary node and stores no operation
            op = node.get_operation()
            if as_two_lists:
                path[0].append(op[0])
                path[1].append(op[1])
            else:
                path.append(op)
            node = node.get_parent()

        rv = path[::-1]
        if ExplorationTree.use_cache:
            self.cache[cache_key] = rv

        return rv


def node_mapping(node, path, ix_from):
    for n in path:
        if n[ix_from] == node:
            return [True, n[1-ix_from]]
    return [False, None]

def update_optimistic_score(et_node, n, score_update):
    """
    Goes up the tree until a node representing mapping of n is encountered. Its score is then
    updated and the change is propagated to all its descendants
    :param et_node:
    :param nb0:
    :param neighbor_score_update:
    :return:
    """

    while et_node.get_operation()[0] != n:
        et_node  = et_node.get_parent()

    propagate_optimistic_score_update(et_node, score_update)


def propagate_optimistic_score_update(et_node, score_update):

    et_node.node_score_optimistic += score_update
    et_node.path_score_optimistic += score_update

    for child in et_node.get_children():
        propagate_optimistic_score_update(child, score_update)

def get_node_repr(node):
    return "[{}-{}]->depth:{}->score:{}->score-opt:{}->total:{}->total-opt:{}".format(node.operation[0],
                                                                                      node.operation[1],
                                                                                      node.get_depth(),
                                                                                      node.get_node_score(),
                                                                                      node.get_node_score_optimistic(),
                                                                                      node.get_path_score(),
                                                                                      node.get_path_score_optimistic())


def get_operation_cost(operation, costs):

    if not operation[0] and not operation[1]:
        return 0

    if not operation[0]:
        return costs.insert(operation)

    if not operation[1]:
        return costs.delete(operation)

    return costs.update(operation)
