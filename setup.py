import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="sged",
    version="0.0.1",
    author="David Hoksza",
    author_email="david.hoksza@gmail.com",
    description="A package implementing semi-global graph edit distance",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/davidhoksza/sged",
    packages=setuptools.find_packages(),
    classifiers=(
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ),
)