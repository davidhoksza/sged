import networkx as nx
from networkx.readwrite import json_graph
import json

from sged import ed_g
from sged import ed as ed
from sged.cost import Costs
import time

# g1 = nx.Graph()
# g1.add_edges_from([['a', 'b']])
# g2 = nx.Graph()
# g2.add_edges_from([['a', 'c']])

#superfluous test
# g1 = nx.Graph()
# g1.add_edges_from([['a', 'b'], ['b', 'c']])
# g2 = nx.Graph()
# g2.add_edges_from([['a', 'b'], ['b', 'c'], ['a','c']])


# g1 = nx.Graph()
# g1.add_edges_from([['a', 'b'], ['b', 'c'], ['b', 'd']])
# g2 = nx.Graph()
# g2.add_edges_from([['c', 'a'], ['c', 'b']])

# g1 = nx.Graph()
# g1.add_edges_from([['x', 'a'], ['a', 'b'], ['b', 'c'], ['b', 'd']])
# g2 = nx.Graph()
# g2.add_edges_from([['x', 'c'], ['c', 'a'], ['c', 'b']])


g1 = nx.Graph()
g1.add_edges_from([['x', 'a'], ['a', 'b'], ['b', 'c'], ['b', 'd']])
g2 = nx.Graph()
g2.add_edges_from([['x', 'c'], ['c', 'a']])


# g1 = nx.Graph()
# g1.add_edges_from([['x', 'c'], ['c', 'a'], ['c', 'b']])
# g2 = nx.Graph()
# g2.add_edges_from([['x', 'a'], ['a', 'b'], ['b', 'c'], ['b', 'd']])


# g1 = nx.Graph()
# g1.add_edges_from([['x', 'a'], ['a', 'b'], ['b', 'c'], ['b', 'd'], ['c', 'e']])
# g2 = nx.Graph()
# g2.add_edges_from([['x', 'c'], ['c', 'a'], ['c', 'b'], ['a', 'e']])


# g1 = nx.Graph()
# g1.add_edges_from([  ['d', 'e'], ['d', 'f'], ['e', 'g'], ['f', 'g']])
# g2 = nx.Graph()
# g2.add_edges_from([ ['d', 'e'], ['e', 'f'], ['e', 'g'], ['f', 'h'], ['g', 'h']])
# path = sged.sged(g2, g1)

# g1 = nx.Graph()
# g1.add_edges_from([['a', 'b'], ['a', 'c'], ['b', 'c'], ['b', 'd'], ['c', 'd'], ['d', 'e'], ['e', 'f'], ['e', 'g'], ['f', 'h'], ['g', 'h']])
# g2 = nx.Graph()
# g2.add_edges_from([['a', 'c'], ['b', 'c'], ['c', 'd'], ['d', 'e'], ['d', 'f'], ['e', 'g'], ['f', 'g']])


# g1 = nx.Graph()
# g1.add_edges_from([['a', 'b'], ['a', 'c'], ['b', 'c'], ['b', 'd'], ['c', 'd'], ['d', 'e'], ['e', 'f'], ['e', 'g'], ['f', 'h'], ['g', 'h']])
# g2 = nx.Graph()
# g2.add_edges_from([['a', 'c'], ['b', 'c'], ['c', 'd'], ['d', 'e'], ['d', 'f'], ['e', 'g'], ['f', 'g']])



# g1 = nx.Graph()
# g1.add_edges_from([['a', 'b'], ['a', 'c'], ['b', 'd'], ['c', 'd'], ['d', 'e'], ['e', 'f'], ['e', 'g'], ['f', 'h'], ['g', 'h']])
# g2 = nx.Graph()
# g2.add_edges_from([['a', 'c'], ['b', 'c'], ['a', 'b'], ['c', 'x'], ['x', 'y'], ['y', 'z'], ['z', 'd'], ['d', 'e'], ['d', 'f'], ['e', 'g'], ['f', 'g']])
# path = ed.sged(g2, g1)

# g1 = nx.Graph()
# g1.add_edges_from([['a', 'b'], ['b', 'c'], ['a', 'c'], ['b', 'd'], ['c', 'd']])
# g2 = nx.Graph()
# g2.add_edges_from([['a', 'b'], ['b', 'c'], ['a', 'c'], ['b', 'd']])
# path = sged.sged(g2, g1, user_settings={"penalize_superfluous_edges": True})

# def update_cost(op):
#     [n1, n2] = op
#     if (n1 == n2 ):
#         return 0
#
#     return 1

# g1 = nx.Graph()
# g1.add_edges_from([['a', 'c'], ['b', 'c'], ['c', 'd'], ['d', 'e'], ['d', 'f']])
# g2 = nx.Graph()
# g2.add_edges_from([['a', 'c'], ['b', 'c'], ['c', 'x'], ['x', 'd'], ['d', 'e'], ['d', 'f']])
# print(ed.sged(g1, g2, node_costs=Costs(insert=lambda x:1, delete=lambda x:1, update=update_cost)))

# def update_cost(op):
#     [n1, n2] = op
#     if (n1 == 'c' and n2 == 'd') or (n1 == 'd' and n2 == 'e') or (n1 == 'b' and n2 == 'c') or (n1 == 'a' and n2 == 'b'):
#         return 0
#
#     return 10
# path = sged.sged(g2, g1, node_costs=Costs(insert=lambda x:10, delete=lambda x:10, update=update_cost), edge_costs=Costs(insert=lambda x:10, delete=lambda x:10))
# path = sged.sged(g2, g1, node_costs=Costs(insert=lambda x:1, delete=lambda x:1), edge_costs=Costs(insert=lambda x:1, delete=lambda x:1))

# for p in path:
#     print(p)
#
# print("{} mappings".format(len(path)))

# def create_graphs(edges):
#     gs = []
#
#     for i in range(2):
#         g = nx.Graph()
#         for e in edges[i]:
#             g.add_edge(e[0], e[1])
#         gs.append(g)
#
#     return gs
#
# [g1, g2] = create_graphs([
#     [['a', 'b'], ['a', 'c'], ['b', 'd'], ['d', 'e'], ['c', 'g'], ['f', 'g'], ['d', 'h'], ['e', 'h'], ['f', 'h'], ['g', 'h']],
#     [['a', 'b'], ['a', 'c'], ['b', 'd'], ['d', 'e'], ['c', 'g'], ['f', 'g'], ['d', 'h'], ['e', 'h'], ['f', 'h'], ['g', 'h']]
# ])


# g1 = nx.Graph()
# g1.add_edges_from([['a', 'b'], ['a', 'c'], ['b', 'd'], ['d', 'e'], ['c', 'g'], ['f', 'g'], ['d', 'h'], ['e', 'h'], ['f', 'h'], ['g', 'h']])
# g2 = nx.Graph()
# g2.add_edges_from([['a', 'b'], ['a', 'c'], ['b', 'd'], ['d', 'e'], ['c', 'g'], ['f', 'g'], ['d', 'h'], ['e', 'h'], ['f', 'h'], ['g', 'h']])

# g1 = nx.Graph()
# g1.add_edges_from([['a', 'b'], ['b', 'c'], ['c', 'd'], ['d', 'e'], ['e', 'f'], ['f', 'a']])
# g2 = nx.Graph()
# g2.add_edges_from([['a', 'b'], ['b', 'c'], ['c', 'd'], ['d', 'e'], ['e', 'f'], ['f', 'a']])

# g1 = nx.Graph()
# g1.add_edges_from([['a', 'b']])
# g2 = nx.Graph()
# g2.add_edges_from([['a', 'x'], ['x', 'y'], ['y', 'b']])

# g1 = nx.Graph()
# g1.add_edges_from([['a','x'], ['a','y'], ['x','y'], ['x','d'], ['y', 'i'], ['i', 'z'], ['z', 'g'], ['g', 'j'], ['j', 'w'], ['w', 'z']])
# g2 = nx.Graph()
# g2.add_edges_from([['a','b'], ['a','c'], ['b','c'], ['b','d'], ['c','d'], ['d','e'], ['d','f'], ['e','f'], ['c','f'], ['f','g'], ['c','h'], ['g','h'], ['g','j'], ['h','k'], ['j','k']])


# When mismatch set to high value and indel to really low, optimal would be a-a;b-None;c-None;None:d;None:d;f-f ... unles number of continuous indels
# g1 = nx.Graph()
# g1.add_edges_from([['a', 'b'], ['b', 'c'], ['c', 'f']])
# g2 = nx.Graph()
# g2.add_edges_from([['a', 'd'], ['d', 'e'], ['e', 'f']])
# print(ed_g.sged(g1, g2, node_costs=Costs(insert=lambda x: 0.01, delete=lambda x: 0.1, update=lambda x: 0 if x[0]==x[1] else 100),
#                 edge_costs= Costs(insert=lambda x: 0, delete=lambda x: 0, update=lambda x: 0),
#                 user_settings={'log_to_console': True, "max_open_set_category_size":-1, "max_children":-1, 'use_adjecency_matrix_scoring': False}))


start = time.time()
# print(ed.sged(g1, g2, user_settings={'log_to_console': True}))
# print(ed_g.sged(g1, g2, user_settings={'log_to_console': True, "max_open_set_category_size":10}))
# print(ed_g.sged(g1, g2, user_settings={'log_to_console': True}, test_ops=[['a', 'b'], ['c', 'd'], ['d', 'e'], ['e', 'f'], ['g', 'h'], ['f', 'g'], ['b', 'c']]))
# print(ed.sged(g1, g2, node_costs=Costs(insert=lambda x:1, delete=lambda x:1, update=lambda x:0 if x[0]==x[1] else 1),
#               user_settings={'log_to_console': True, "max_open_set_size": -1}))
print(ed_g.sged(g1, g2, node_costs=Costs(insert=lambda x: 1, delete=lambda x: 1, update=lambda x: 0 if x[0]==x[1] else 1),
                user_settings={'log_to_console': True, "max_open_set_category_size":-1, "max_children":-1, 'use_adjecency_matrix_scoring': True}))

print("Time: {} s".format(time.time() - start))

# print(ed_g.sged(g1, g2, node_costs=Costs(insert=lambda x:0.01, delete=lambda x:1, update=lambda x:0 if x[0]==x[1] else 1),
#                 edge_costs=Costs(insert=lambda x:0.01, delete=lambda x:0.01),
#                 user_settings={'log_to_console': True, "max_open_set_category_size":-1}))