import networkx as nx
import numpy as np
from collections import Counter

from sged.cost import Costs

def node_dist(n1, n2):
    #nodes are reactions so it should take into account
    #type of reaction (especially association or dissociation might be generally different from state transition?
    if n1 and not n2:
        return 4
    if not n1 and n2:
        return 4
    else:
        return 0

def edge_dist(e1, e2):
    return 4


def extract_neighbors(g, g_orig):
    ns = {}
    for n in g.nodes():
        ns[n] = list(nx.all_neighbors(g_orig, n))
    return ns

def dist_neighbors(n1, nbs1, g1, n2, nbs2, g2):

    c1 = Counter([g1[n1][n]["role"] + str(g1.nodes[n]["sboTerm"]) for n in nbs1])
    c2 = Counter([g2[n2][n]["role"] + str(g2.nodes[n]["sboTerm"]) for n in nbs2])
    # c1 = Counter([str(g1.nodes[n]["sboTerm"]) for n in nbs1])
    # c2 = Counter([str(g2.nodes[n]["sboTerm"]) for n in nbs2])
    c1.subtract(c2)

    # d = sum([(abs(x)) for x in c1.values()])
    d = sum([(max(x, 0)) for x in c1.values()]) #if n1 is subset of n2 then distance = 0
    return 1-1/(d+0.1) if d > 0 else 0 #d+0.1 because if d=1, the distance would be 0


def get_sim_matrix(g1, g1_orig, g2, g2_orig):
    neighbors = [extract_neighbors(g1, g1_orig), extract_neighbors(g2, g2_orig)]

    nodes1 = list(g1.nodes())
    nodes2 = list(g2.nodes())

    matrix = np.empty([len(nodes1), len(nodes2)])
    for i1 in range(len(nodes1)):
        ns1 = list(neighbors[0][nodes1[i1]])
        for i2 in range(len(nodes2)):
            ns2 = list(neighbors[1][nodes2[i2]])
            matrix[i1,i2] = dist_neighbors(nodes1[i1], ns1, g1_orig, nodes2[i2], ns2, g2_orig)

    return matrix


class LayoutNodeCosts(Costs):

    def __init__(self, g1, g1_orig, g2, g2_orig):
        self.node_dict = [{}, {}]
        for [g, node_dict] in [[g1, self.node_dict[0]], [g2, self.node_dict[1]]]:
            i = 0
            for n in g.nodes():
                node_dict[n] = i
                i += 1

        self.sim_matrix = get_sim_matrix(g1, g1_orig.to_undirected(), g2, g2_orig.to_undirected())
        self.mean_update_cost = np.mean(self.sim_matrix)

    def insert(self, operation):
        # return self.mean_update_cost
        return 3

    def update(self, operation):
        return self.sim_matrix[self.node_dict[0][operation[0]], self.node_dict[1][operation[1]]]

    def delete(self, operation):
        # return self.mean_update_cost
        return 3

class LayoutEdgeCosts(Costs):

    def __init__(self, g1, g1_orig, g2, g2_orig):
        self.sim_matrix = get_sim_matrix(g1, g1_orig.to_undirected(), g2, g2_orig.to_undirected())
        self.mean_update_cost = np.mean(self.sim_matrix)

    def insert(self, operation):
        # return self.mean_update_cost / 10
        return 0.2

    def update(self, operation):
        return 0

    def delete(self, operation):
        # return self.mean_update_cost / 10
        return 0.2
