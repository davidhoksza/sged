import networkx as nx
from networkx.readwrite import json_graph
import json


import sys
sys.path.insert(0,'../..')

from sged import ed_g
from sged import ed as ed
from sged.cost import Costs
import scoring
import graph_utils as gu

import time


# def get_largest_cc(f_name):
#     def get_normalized_copy(g):
#         new_g = nx.DiGraph() if g.is_directed() else nx.Graph()  # TODO: other graph types
#         new_g.add_nodes_from(sorted(list(g.nodes(data=True))))
#         new_g.add_edges_from(sorted(list(g.edges(data=True))))
#         return new_g
#
#     with open(f_name, 'tr') as f:
#         data = json.load(f)
#         g = json_graph.node_link_graph(data)
#         ccs = sorted(list(nx.algorithms.components.connected_components(g.to_undirected())), key=lambda x:len(x), reverse=True)
#         return get_normalized_copy(g.subgraph(ccs[0]))


def get_identity_mapping(g1, g2):
    m = []
    for n in g1:
        n2 = n if n in g2 else None
        m.append([n, n2])
    return m


# g1 = get_largest_cc('data/Axonal_remodeling_remove_species_abs_5-rg.json')
# g1_full = get_largest_cc('data/Axonal_remodeling_remove_species_abs_5.json')
# g2 = get_largest_cc('data/pdmap-Axonal_remodeling-rg.json')
# g2_full = get_largest_cc('data/pdmap-Axonal_remodeling.json')

def get_full_and_ddup_rg(fname):
    g_full = gu.load_graph(fname, 'json')
    g_rg = gu.get_reaction_graph(g_full)
    g_rg_ddup = gu.get_ddup_reaction_graph(g_rg, g_full)
    g = sorted(gu.get_connected_components(g_rg_ddup), key=lambda x:len(x), reverse=True)[0]
    return g_full, g


# g1 = get_largest_cc('data/pdmap-Ubiquitin Proteasome System-rg.json')
# g1_full, g1 = get_full_and_ddup_rg('data/pdmap-Ubiquitin Proteasome System.json')
# g2_full, g2 = get_full_and_ddup_rg('data/Ubiquitin Proteasome System_remove_species_abs_20.json')

g1_full, g1 = get_full_and_ddup_rg('data/muscle_map_seed.json')
g2_full, g2 = get_full_and_ddup_rg('data/pdmap.json')


node_costs = scoring.LayoutNodeCosts(g1, g1_full, g2, g2_full)
edge_costs = scoring.LayoutEdgeCosts(g1, g1_full, g2, g2_full)
# node_costs = scoring.LayoutNodeCosts(g2, g2_full, g1, g1_full)
# edge_costs = scoring.LayoutEdgeCosts(g2, g2_full, g1, g1_full)


start = time.time()
print(ed_g.sged(g1, g2, node_costs=node_costs, edge_costs=edge_costs,
                user_settings={'log_to_console': True

                    ,'use_adjecency_matrix_scoring': False
                    ,'switch_if_first_bigger':True

                    ,'seed_pairs_count': 100, 'max_open_set_category_sizes': [100,10], 'max_children': 5

                    ,'max_time_in_s': 10, 'must_find_solution': True}
                # , test_ops=get_identity_mapping(g2, g1)
                ))

print("Time: {} s".format(time.time() - start))
