#testing with py.test
from sged import ed
# from sged import ed_g as ed
import networkx as nx


def check_res(res, distance, length):
    assert (len(res) == length)
    for r in res:
        assert (r['distance'] == distance)


def test_sged_identical_graphs():
    g1 = nx.Graph()
    g1.add_edges_from([['a', 'b']])
    g2 = nx.Graph()
    g2.add_edges_from([['a', 'b']])

    res = ed.sged(g1, g2)

    assert (len(res) == 2)
    assert(res[0]['distance'] == 0 and res[1]['distance'] == 0)
    mappings = [r['mapping'] for r in res]
    assert([['a','a'], ['b', 'b']] in mappings)


def test_sged_different_labels_same_topology_graph():

    g1 = nx.Graph()
    g1.add_edges_from([['a', 'b']])
    g2 = nx.Graph()
    g2.add_edges_from([['a', 'c']])

    res = ed.sged(g1, g2)
    print(res)

    check_res(res, 0, 2)


def test_sged_fork_with_longer_stem_in_one():

    g1 = nx.Graph()
    g1.add_edges_from([['x', 'a'], ['a', 'b'], ['b', 'c'], ['b', 'd'], ['c', 'e']])
    g2 = nx.Graph()
    g2.add_edges_from([['x', 'c'], ['c', 'a'], ['c', 'b'], ['a', 'e']])

    res = ed.sged(g2, g1)

    check_res(res, 0, 4)


def test_sged_fork_diamond_vs_diamond_with_horizontal_line_diamond():
    g1 = nx.Graph()
    g1.add_edges_from([['a', 'c'], ['b', 'c'], ['c', 'd'], ['d', 'e'], ['d', 'f'], ['e', 'g'], ['f', 'g']])
    g2 = nx.Graph()
    g2.add_edges_from([['a', 'b'], ['a', 'c'], ['b', 'c'], ['b', 'd'], ['c', 'd'], ['d', 'e'], ['e', 'f'], ['e', 'g'], ['f', 'h'], ['g', 'h']])

    res = ed.sged(g1, g2)

    check_res(res, 1, 8)


# def test_sged_diamond_diamond_vs_triangle_path_diamond():
#     g1 = nx.Graph()
#     g1.add_edges_from([['a', 'c'], ['b', 'c'], ['c', 'd'], ['d', 'e'], ['d', 'f'], ['e', 'g'], ['f', 'g']])
#     g2 = nx.Graph()
#     g2.add_edges_from(
#         [['a', 'b'], ['a', 'c'], ['b', 'c'], ['b', 'd'], ['c', 'd'], ['d', 'e'], ['e', 'f'], ['e', 'g'], ['f', 'h'],
#          ['g', 'h']])
#
#     res = ed.sged(g1, g2)
#
#     check_res(res, 1, 8)
#
#
# def test_sged_traingle_path_cross_diamond_vs_damond_crossed_diamond():
#
#     g1 = nx.Graph()
#     g1.add_edges_from(
#         [['a', 'c'], ['b', 'c'], ['a', 'b'], ['c', 'x'], ['x', 'y'], ['y', 'z'], ['z', 'd'], ['d', 'e'], ['d', 'f'],
#          ['e', 'g'], ['f', 'g'], ['e', 'f'], ['d', 'g']])
#     g2 = nx.Graph()
#     g2.add_edges_from(
#         [['a', 'b'], ['a', 'c'], ['b', 'd'], ['c', 'd'], ['d', 'e'], ['e', 'f'], ['e', 'g'], ['f', 'h'], ['g', 'h'], ['f', 'g'], ['e', 'h']])
#
#
#     res = ed.sged(g1, g2)
#
#     check_res(res, 9, 12)

def test_sfg_superfluous_edges_diamond_with_horizontal_vs_one_edge_removed():

    g1 = nx.Graph()
    g1.add_edges_from([['a', 'b'], ['b', 'c'], ['a', 'c'], ['b', 'd']])
    g2 = nx.Graph()
    g2.add_edges_from([['a', 'b'], ['b', 'c'], ['a', 'c'], ['b', 'd'], ['c', 'd']])

    res_true = ed.sged(g1, g2, user_settings={"penalize_superfluous_edges": True})
    res_false = ed.sged(g1, g2, user_settings={"penalize_superfluous_edges": False})

    assert(res_true[0]['distance'] == 1 and res_false[1]['distance'] == 0)





